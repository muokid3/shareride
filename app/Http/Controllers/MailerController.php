<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailerController extends Controller
{
    //

    public function rideBooked($user, $ride)
    {
        Mail::send('mailer.ride_book', ['user' => $user, 'ride' => $ride], function ($message) use ($user) {
            $subject = "You Booked a Ride!";
            $message->to($user->email, $user->name)
                ->subject($subject);
        });
    }
}
