<?php

namespace App\Http\Controllers;

use App\Ride;
use App\RideBooking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RideController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function give_ride()
    {
        return view('give_ride');
    }

    public function get_ride()
    {
        $rides = DB::table('rides')
            ->whereNotIn('driver_id', [Auth::user()->id])
            ->get();


        return view('get_ride')->with(compact('rides'));
    }

    public function booked_rides()
    {
        return view('booked_rides');
    }

    public function create_ride(Request $request)
    {
        $this->validate($request, [
            'origin' => 'required',
            'destination' => 'required',
            'date' => 'required',
            'capacity' => 'required'
        ]);
        DB::transaction(function () {
            $request = (object)$_POST;

            $ride = new Ride();

            $ride->driver_id = Auth::user()->id;
            $ride->origin = $request->origin;
            $ride->destination = $request->destination;
            $ride->capacity = $request->capacity;
            $ride->date = date('Y-m-d', strtotime($request->date));


                if ($data = $ride->saveOrFail()) {
                    Session::flash("success", "Ride created successfully");
                }else {
                    Session::flash('error', 'Sorry. The ride could not be created. Please try again.');
                }



        });

        return redirect('/profile');
    }

    public function book_ride($ride_id)
    {
        DB::transaction(function () {
            $request = (object)$_GET;

            $ride = Ride::find($request->ride_id);
            $ride->booked = 1;

            $ride_booking = new RideBooking();
            $ride_booking->ride_id = $request->ride_id;
            $ride_booking->driver_id = $ride->driver_id;
            $ride_booking->passenger_id = Auth::user()->id;

                if ($ride->update() && $ride_booking->save()) {
                    //send mail
                    (new MailerController())->rideBooked(Auth::user(), $ride);
                    Session::flash("success", "Ride booking successful");
                }else {
                    Session::flash('error', 'Sorry. There was a problem booking this ride. Please try again.');
                }
        });

    }
}

