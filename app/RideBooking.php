<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RideBooking extends Model
{
    //

    public function ride()
    {
        return $this->belongsTo('App\Ride');
    }
    public function driver()
    {
        return $this->belongsTo('App\User', 'driver_id');
    }
}
