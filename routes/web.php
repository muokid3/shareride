<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/about', 'HomeController@about');
Route::get('/pricing', 'HomeController@pricing');
Route::get('/contact', 'HomeController@contact');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'UserController@index');
    Route::get('/give_ride', 'RideController@give_ride');
    Route::get('/get_ride', 'RideController@get_ride');
    Route::get('/booked_rides', 'RideController@booked_rides');
    Route::post('/create_ride', 'RideController@create_ride');
    Route::get('/ride/book/{ride_id}', 'RideController@book_ride');


});
