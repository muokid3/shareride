<?php $page_title = "About"; ?>

@extends('layouts.app')
@section('css')
    <style>
        #photo-container {
            height: 150px;
            width:100%;
            margin-bottom: 20px;
        }

        #photo {
            height: 150px;
            margin: 0 auto;
            width: 150px;
        }

        @media (max-width: 300px) {
            #photo-container,
            #photo {
                height: 40px;
                width: 40px;
            }
        }
    </style>
@endsection
@section('content')

<aside id="fh5co-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(images/img_bg_4.jpg);">
                <div class="overlay-gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-xs-12 text-center slider-text">
                            <div class="slider-text-inner">

                                <div id="photo-container">
                                    <img class="img-circle img-responsive" src="{!! url('images/default.jpg') !!}" id="photo">
                                </div>
                                {{--<img id="profImage" src="" class="img-circle img-responsive" width="60px" height="60px">--}}
                                <h1 id="displayName" class="heading-section">{{Auth::user()->name}}</h1>
                                <h2 id="myEmail">{{Auth::user()->email}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>

<div id="fh5co-course-categories">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">

                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif

                <h2>Booked Rides</h2>
                <p>Hey, here is a list of rides that you have booked on this platform</p>
            </div>
        </div>

        <div class="col-md-6 col-md-offset-3 animate-box">
            <h3>Rides I Booked</h3>
            <table class="table tabe-responsive">
                <thead>
                <tr>
                    <th class="hidden-xs">Date</th>
                    <th>From</th>
                    <th>To</th>
                    <th>Capacity</th>
                    <th class="hidden-xs">Driver</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>

                @foreach(Auth::user()->booked_rides as $ride_booking)
                    <tr>
                        <td class="hidden-xs">{{$ride_booking->ride->date}}</td>
                        <td>{{$ride_booking->ride->origin}}</td>
                        <td>{{$ride_booking->ride->destination}}</td>
                        <td>{{$ride_booking->ride->capacity}}</td>
                        <td class="hidden-xs">{{$ride_booking->driver->name}}</td>
                        <td>{{strtotime($ride_booking->ride->date) > strtotime(\Carbon\Carbon::now()->format('Y-m-d')) ? "Upcoming" : "Past" }}</td>
                    </tr>
                @endforeach


                </tbody>
            </table>

        </div>

    </div>
</div>

@endsection


