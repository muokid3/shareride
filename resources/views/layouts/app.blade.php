<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Shareride | {{isset($page_title)?$page_title : ''}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Shareride" />
    <meta name="keywords" content="Shareride" />
    <meta name="author" content="Dennis Muoki" />


    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="{{url('css/animate.css')}}">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="{{url('css/icomoon.css')}}">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="{{url('css/bootstrap.css')}}">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="{{url('css/magnific-popup.css')}}">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="{{url('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('css/owl.theme.default.min.css')}}">

    <!-- Flexslider  -->
    <link rel="stylesheet" href="{{url('css/flexslider.css')}}">

    <!-- Pricing -->
    <link rel="stylesheet" href="{{url('css/pricing.css')}}">

    <!-- Theme style  -->
    <link rel="stylesheet" href="{{url('css/style.css')}}">


    <!-- Modernizr JS -->
    <script src="{{url('js/modernizr-2.6.2.min.js')}}"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="{{ url('js/respond.min.js')}}"></script>


    <![endif]-->

    @yield('css')


</head>

<body>
<div class="fh5co-loader"></div>

<div id="page">
@include('common.header')
@yield('content')
@include('common.footer')
</div>
</body>
<!-- jQuery -->
<script src="{{url('js/jquery.min.js')}}"></script>
<!-- jQuery Easing -->
<script src="{{url('js/jquery.easing.1.3.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Bootstrap -->
<script src="{{url('js/bootstrap.min.js')}}"></script>
<script src="{{url('js/bootbox.min.js')}}"></script>                                  <!-- Load Bootbox -->

<!-- Waypoints -->
<script src="{{url('js/jquery.waypoints.min.js')}}"></script>
<!-- Stellar Parallax -->
<script src="{{url('js/jquery.stellar.min.js')}}"></script>
<!-- Carousel -->
<script src="{{url('js/owl.carousel.min.js')}}"></script>
<!-- Flexslider -->
<script src="{{url('js/jquery.flexslider-min.js')}}"></script>
<!-- countTo -->
<script src="{{url('js/jquery.countTo.js')}}"></script>
<!-- Magnific Popup -->
<script src="{{url('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('js/magnific-popup-options.js')}}"></script>
<!-- Count Down -->
<script src="{{url('js/simplyCountdown.js')}}"></script>
<!-- Main -->
<script src="{{url('js/main.js')}}"></script>
<script type="text/javascript" src="{{ url('/js/moment.js')}}"></script>

<script src="{{ url('/js/datetimepicker.js') }}"></script>

@yield('scripts')
<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>
</html>

