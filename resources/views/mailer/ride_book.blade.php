<html>
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
</head>
<body leftmargin="0" marginheight="0" marginwidth="0" offset="0"
      style='width:100%;color:#363636;background-color:#F2F2F2;font-family:"Helvetica Neue",Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;' topmargin="0">
<center>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
            <td align="center" style="padding-bottom:40px;" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" style="margin-top:40px;margin-left:10px;margin-right:10px;border: 1px solid #e3e3e3;background-color: #FFFFFF;" width="650">
                    <tr>
                        <td width="30">
                            &nbsp;
                        </td>
                        <td align="center" height="90" style="border-bottom: 1px solid #eeeeee;" valign="middle">
                            <img src="http://www.dailycarshare.com/img/banner.jpg" height="60px" width="360px">
                        </td>
                        <td width="30">
                            &nbsp;
                        </td>
                    </tr>
                    <!-- /START BODY -->
                    <tr>
                        <td style="padding-top:30px;padding-bottom:30px;border-bottom: 1px solid #eeeeee;" width="30">
                            &nbsp;
                        </td>
                        <td align="left" style="font-size:14px;padding-top:30px;padding-bottom:30px;border-bottom: 1px solid #eeeeee;" valign="top">
                            <p style="margin:0px;line-height:24px">
                                <b>Hello {{$user->name}}!</b>
                                <br>
                                <br>
                                Congratulations! You have successfully booked a ride with Shareride!
                                <br>
                            <table>

                                <tr>
                                    <td>Date: </td>
                                    <td>{{\Carbon\Carbon::parse(date("d-m-Y", strtotime($ride->date)))->formatLocalized('%A %d %B %Y')}}</td>
                                </tr>

                                <tr>
                                    <td>Origin: </td>
                                    <td>
                                            {{$ride->origin}}
                                    </td>
                                </tr>

                                <tr>
                                    <td>Destination: </td>
                                    <td>{{$ride->destination}}</td>
                                </tr>
                            </table>
                            <p>
                                <br>
                                <br>
                                Your driver will be {{\App\User::find($ride->driver_id)? \App\User::find($ride->driver_id)->name : ""}}.
                                Please contact them via email at {{\App\User::find($ride->driver_id)? \App\User::find($ride->driver_id)->email : ""}} for further logistics.
                                <br>
                                <br>
                            </p>
                            <br>
                            All the best in your trip!
                            <br>
                            <br>
                            Shareride Inc.<br>
                            Central Command Center<br>
                            Tel: +254 777 777 777
                            <br>
                            <br>
                            Shareride Towers 10th Floor, Funk Avenue, Nairobi<br>
                            <br>
                            ----------------------------------------------------------------
                            <br>
                            <br>
                            <br>
                            </p>
                        </td>
                        <td style="padding-top:30px;padding-bottom:30px;border-bottom: 1px solid #eeeeee;" width="30">
                            &nbsp;
                        </td>
                    </tr>
                    <!-- /END BODY -->
                </table>
            </td>
        </tr>
    </table>
</center>
</body>
</html>
