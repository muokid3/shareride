<?php $page_title = "About"; ?>

@extends('layouts.app')
@section('css')
    <style>
        #photo-container {
            height: 150px;
            width:100%;
            margin-bottom: 20px;
        }

        #photo {
            height: 150px;
            margin: 0 auto;
            width: 150px;
        }

        @media (max-width: 300px) {
            #photo-container,
            #photo {
                height: 40px;
                width: 40px;
            }
        }
    </style>
@endsection
@section('content')

<aside id="fh5co-hero">
    <div class="flexslider">
        <ul class="slides">
            <li style="background-image: url(images/img_bg_4.jpg);">
                <div class="overlay-gradient"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-xs-12 text-center slider-text">
                            <div class="slider-text-inner">

                                <div id="photo-container">
                                    <img class="img-circle img-responsive" src="{!! url('images/default.jpg') !!}" id="photo">
                                </div>
                                {{--<img id="profImage" src="" class="img-circle img-responsive" width="60px" height="60px">--}}
                                <h1 id="displayName" class="heading-section">{{Auth::user()->name}}</h1>
                                <h2 id="myEmail">{{Auth::user()->email}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</aside>

<div id="fh5co-course-categories">
    <div class="container">
        <div class="row animate-box">

            <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif


                <h2>Give a ride</h2>
                <p>Hey, you got a ride? Share with a friend and get some extra bucks</p>
            </div>
        </div>

        <div class="col-md-6 col-md-offset-3 animate-box">
            <h3>Ride Details</h3>
            <form action="{{url('/create_ride')}}" method="post">
                {{ csrf_field() }}
                <div class="row form-group">
                    <div class="col-md-12">
                        <!-- <label for="fname">First Name</label> -->
                        {{--<input type="text" id="date" name="date" class="form-control date" placeholder="Date">--}}
                        <div class='input-group date' id='datetimepicker1'>
                            <input name="date" type='text' class="form-control" />
                            <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                        </div>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12">
                        <!-- <label for="email">Email</label> -->
                        <input type="text" id="origin" name="origin" class="form-control" placeholder="Origin">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12">
                        <!-- <label for="subject">Subject</label> -->
                        <input type="text" id="destination" name="destination" class="form-control" placeholder="Destination">
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-12">
                        <!-- <label for="subject">Subject</label> -->
                        <input type="number" id="capacity" name="capacity" class="form-control" placeholder="Capacity">
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Share" class="btn btn-primary">
                </div>

            </form>
        </div>



    </div>
</div>

@endsection


@section("scripts")


    <script>

        $(function () {
            var bindDatePicker = function() {
                $(".date").datetimepicker({
                    format:'YYYY-MM-DD',
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    }
                }).find('input:first').on("blur",function () {
                    // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
                    // update the format if it's yyyy-mm-dd
                    var date = parseDate($(this).val());

                    if (! isValidDate(date)) {
                        //create date based on momentjs (we have that)
                        date = moment().format('YYYY-MM-DD');
                    }

                    $(this).val(date);
                });
            }

            var isValidDate = function(value, format) {
                format = format || false;
                // lets parse the date to the best of our knowledge
                if (format) {
                    value = parseDate(value);
                }

                var timestamp = Date.parse(value);

                return isNaN(timestamp) == false;
            }

            var parseDate = function(value) {
                var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
                if (m)
                    value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

                return value;
            }

            bindDatePicker();
        });
    </script>
@endsection


