




<nav class="fh5co-nav" role="navigation">
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <p class="site">www.shareride.com</p>
                        <p class="num">Call: +254 123 456 7890</p>
                        <ul class="fh5co-social">
                            <li><a href="#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="#"><i class="icon-dribbble2"></i></a></li>
                            <li><a href="#"><i class="icon-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="fh5co-logo"><a href="{{url('/')}}"><i class="icon-cog"></i>Shareride<span>.</span></a></div>                    </div>
                    <div  class=" col-xs-10 text-right menu-1">
                        <ul>


                            <li class="active" ><a href="{{url('/')}}">Home</a></li>
                            @if(@Auth::guest())
                                <li class="btn-cta"><a href="{{'login'}}"><span>Login</span></a></li>
                                <li class="btn-cta"><a href="{{'register'}}"><span>Register</span></a></li>
                            @else
                                <li class="has-dropdown">
                                    <a href="#">Rides</a>
                                    <ul class="dropdown">
                                        <li><a href="{{url('/give_ride')}}">Give a Ride</a></li>
                                        <li><a href="{{url('/get_ride')}}">Get a Ride</a></li>
                                        <li><a href="{{url('/profile')}}">My Rides</a></li>
                                        <li><a href="{{url('/booked_rides')}}">Booked Rides</a></li>
                                    </ul>
                                </li>

                                <li class="btn-cta"><a href="{{'profile'}}"><span>{{Auth::user()->name}}</span></a></li>
                                <li class="btn-cta"><a href="{{'logout'}}"><span>Logout</span></a></li>
                            @endif



                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </nav>








