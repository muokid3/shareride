<?php $page_title = "Welcome"; ?>

@extends('layouts.app')

@section('content')

    <aside id="fh5co-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/img_bg_1.jpg);">
                    <div class="overlay-gradient"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center slider-text">
                                <div class="slider-text-inner">
                                    <h1>Welcome to Shareride</h1>
                                    <h2>Lets get you moving</h2>
                                    <p>
                                        <a class="btn btn-primary btn-lg" href="{{url('get_ride')}}">Get a Ride Now!</a>
                                    </p>
                                    <h2>-OR-</h2>
                                    <p>
                                        <a class="btn btn-primary btn-lg" href="{{url('give_ride')}}">Share your Ride!</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div id="fh5co-course-categories">
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
                    <h2>Rides for you</h2>
                    <p>Here is a sneak peak of some of the available rides we have for you</p>
                </div>
            </div>
            <div class="row">

                @foreach(\App\Ride::take(4)->orderBy('id','desc')->get() as $ride)
                    @if(strtotime($ride->date) > strtotime(\Carbon\Carbon::now()->format('Y-m-d')))
                        <div class="col-md-3 col-sm-6 text-center animate-box">
                            <div class="services">
						<span class="icon">
							<i class="icon-anchor"></i>
						</span>
                                <div class="desc">
                                    <h4>
                                        <a href="{{url('get_ride')}}">
                                            {{\Carbon\Carbon::parse(date("d-m-Y", strtotime($ride->date)))->formatLocalized('%A %d %B %Y')}}
                                        </a>
                                    </h4>
                                    <h3>{{$ride->origin}} to {{$ride->destination}}</h3>
                                </div>

                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

    <div id="fh5co-counter" class="fh5co-counters" style="background-image: url(images/img_bg_4.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 text-center animate-box">
                            <span class="icon"><i class="icon-world"></i></span>
                            <span class="fh5co-counter js-counter" data-from="0" data-to="100" data-speed="5000" data-refresh-interval="50"></span>
                            <span class="fh5co-counter-label">Rides</span>
                        </div>
                        <div class="col-md-4 col-sm-6 text-center animate-box">
                            <span class="icon"><i class="icon-bulb"></i></span>
                            <span class="fh5co-counter js-counter" data-from="0" data-to="3700" data-speed="5000" data-refresh-interval="50"></span>
                            <span class="fh5co-counter-label">Riders</span>
                        </div>

                        <div class="col-md-4 col-sm-6 text-center animate-box">
                            <span class="icon"><i class="icon-head"></i></span>
                            <span class="fh5co-counter js-counter" data-from="0" data-to="1080" data-speed="5000" data-refresh-interval="50"></span>
                            <span class="fh5co-counter-label">Drivers</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection